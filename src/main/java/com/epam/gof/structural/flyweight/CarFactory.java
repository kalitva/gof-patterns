package com.epam.gof.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

public class CarFactory {
    private static final Map<String, Car> cash = new HashMap<>();

    private CarFactory() {
    }

    @SuppressWarnings({"java:S106", "UnusedReturnValue"})
    public static Car createCar(String color) {
        if (cash.containsKey(color)) {
            return cash.get(color);
        }
        Car car = new Car(color, new Engine());
        cash.put(color, car);
        System.out.println("Created " + color + " car...");

        return car;
    }
}
