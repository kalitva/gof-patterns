package com.epam.gof.structural.flyweight;

@SuppressWarnings("java:S106")
public class Engine {

    public Engine() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }

    public void start() {
        System.out.println("Br.. br...");
    }

    public void stop() {
        System.out.println("Bzhzhzhz...");
    }
}
