package com.epam.gof.structural.flyweight;

public class Client {

    public static void main(String[] args) {
        CarFactory.createCar("red");
        CarFactory.createCar("red");
        CarFactory.createCar("red");
        CarFactory.createCar("green");
        CarFactory.createCar("green");
    }
}
