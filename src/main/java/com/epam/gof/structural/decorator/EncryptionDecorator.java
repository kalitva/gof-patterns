package com.epam.gof.structural.decorator;

import java.util.Base64;

public class EncryptionDecorator implements DataSource {
    private final DataSource dataSource;

    public EncryptionDecorator(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void writeData(String data) {
        dataSource.writeData(encode(data));
    }

    @Override
    public String readData() {
        return decode(dataSource.readData());
    }

    private String encode(String data) {
        return Base64.getEncoder().encodeToString(data.getBytes());
    }

    private String decode(String data) {
        return new String(Base64.getDecoder().decode(data));
    }
}
