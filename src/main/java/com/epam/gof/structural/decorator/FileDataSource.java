package com.epam.gof.structural.decorator;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileDataSource implements DataSource {
    private final String filename;

    public FileDataSource(String name) {
        this.filename = name;
    }

    @Override
    public void writeData(String data) {
        try {
            Files.write(Path.of(filename), data.getBytes());
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    @Override
    public String readData() {
        try {
            return Files.readString(Path.of(filename));
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }
}
