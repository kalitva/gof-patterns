package com.epam.gof.structural.decorator;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.Base64;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

public class CompressionDecorator implements DataSource {
    private static final int DEFAULT_COMPRESS_LEVEL = 6;

    private final DataSource dataSource;

    public CompressionDecorator(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void writeData(String data) {
        dataSource.writeData(compress(data));
    }

    @Override
    public String readData() {
        return decompress(dataSource.readData());
    }

    private String compress(String stringData) {
        try (ByteArrayOutputStream bout = new ByteArrayOutputStream(512)) {
            DeflaterOutputStream dos = new DeflaterOutputStream(bout, new Deflater(DEFAULT_COMPRESS_LEVEL));
            dos.write(stringData.getBytes());
            dos.close();
            return Base64.getEncoder().encodeToString(bout.toByteArray());
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }

    private String decompress(String stringData) {
        try (InputStream in = new ByteArrayInputStream(Base64.getDecoder().decode(stringData));
             InflaterInputStream iin = new InflaterInputStream(in);
             ByteArrayOutputStream bout = new ByteArrayOutputStream(512)) {
            bout.write(iin.readAllBytes());
            return bout.toString();
        } catch (IOException ex) {
            throw new UncheckedIOException(ex);
        }
    }
}
