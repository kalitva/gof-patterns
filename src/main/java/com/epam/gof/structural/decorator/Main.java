package com.epam.gof.structural.decorator;

/**
 * @see java.io.InputStream
 * @see java.io.OutputStream
 */
public class Main {

    public static void main(String[] args) {
        String salaryRecords = "Name,Salary\nJohn Smith,100000\nSteven Jobs,912000";

        DataSource encoded = new CompressionDecorator(
                new EncryptionDecorator(new FileDataSource("OutputDemo.txt")));
        encoded.writeData(salaryRecords);

        DataSource plain = new FileDataSource("OutputDemo.txt");

        System.out.println("---------------- Input --------------");
        System.out.println(salaryRecords);
        System.out.println();

        System.out.println("-------------- Encoded --------------");
        System.out.println(plain.readData());
        System.out.println();

        System.out.println("-------------- Decoded --------------");
        System.out.println(encoded.readData());
        System.out.println();
    }
}
