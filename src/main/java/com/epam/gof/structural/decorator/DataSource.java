package com.epam.gof.structural.decorator;

public interface DataSource {

    void writeData(String data);

    String readData();
}
