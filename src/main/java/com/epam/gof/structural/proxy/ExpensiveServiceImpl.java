package com.epam.gof.structural.proxy;

import java.util.stream.IntStream;

@SuppressWarnings("java:S106")
public class ExpensiveServiceImpl implements ExpensiveService {

    public ExpensiveServiceImpl() {
        heavyInit();
    }

    @Override
    public void doService() {
        System.out.println("Do service");
    }

    private void heavyInit() {
        IntStream.range(0, 10).forEach(i -> {
            System.out.println("Initializing...");
            try {
                Thread.sleep(300);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        });
    }
}
