package com.epam.gof.structural.proxy;

public class Client {

    public static void main(String[] args) {
        ExpensiveService expensiveService = new ExpensiveServiceProxy();
        expensiveService.doService();
        expensiveService.doService();
    }
}
