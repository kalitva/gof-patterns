package com.epam.gof.structural.proxy;

public class ExpensiveServiceProxy implements ExpensiveService {
    private ExpensiveService expensiveService;

    @Override
    public void doService() {
        if (expensiveService == null) {
            expensiveService = new ExpensiveServiceImpl();
        }
        expensiveService.doService();
    }
}
