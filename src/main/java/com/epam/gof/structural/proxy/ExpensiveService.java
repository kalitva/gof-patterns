package com.epam.gof.structural.proxy;

public interface ExpensiveService {

    void doService();
}
