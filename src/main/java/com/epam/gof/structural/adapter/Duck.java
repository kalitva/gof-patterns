package com.epam.gof.structural.adapter;

public interface Duck {

    void quack();

    void fly();
}
