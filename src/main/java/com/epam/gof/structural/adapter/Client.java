package com.epam.gof.structural.adapter;

import java.util.Enumeration;

/**
 * @see java.util.Arrays#asList 
 * @see Enumeration#asIterator
 */
public class Client {

    @SuppressWarnings("java:S106")
    public static void main(String[] args) {
        Duck duck = new DuckImpl();
        Turkey turkey = new TurkeyImpl();
        Duck turkeyAdapter = new TurkeyAdapter(turkey);

        System.out.println("----Turkey");
        turkey.gobble();
        turkey.fly();
        System.out.println();

        System.out.println("----Duck");
        duck.quack();
        duck.fly();
        System.out.println();

        System.out.println("----Turkey Adapter");
        turkeyAdapter.quack();
        turkeyAdapter.fly();
        System.out.println();
    }
}
