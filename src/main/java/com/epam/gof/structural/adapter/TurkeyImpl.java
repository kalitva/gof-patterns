package com.epam.gof.structural.adapter;

@SuppressWarnings("java:S106")
public class TurkeyImpl implements Turkey {

    @Override
    public void gobble() {
        System.out.println("Gobble gobble...");
    }

    @Override
    public void fly() {
        System.out.println("Short distance flight...");
    }
}
