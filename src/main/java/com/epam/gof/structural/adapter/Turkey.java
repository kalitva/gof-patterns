package com.epam.gof.structural.adapter;

public interface Turkey {

    void gobble();

    void fly();
}
