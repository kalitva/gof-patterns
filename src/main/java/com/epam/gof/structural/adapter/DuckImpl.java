package com.epam.gof.structural.adapter;

@SuppressWarnings("java:S106")
public class DuckImpl implements Duck {

    @Override
    public void quack() {
        System.out.println("Quack quack...");
    }

    @Override
    public void fly() {
        System.out.println("Fly...");
    }
}
