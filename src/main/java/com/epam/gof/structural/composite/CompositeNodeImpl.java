package com.epam.gof.structural.composite;

import java.util.ArrayList;
import java.util.List;

public class CompositeNodeImpl implements CompositeNode {
    private final List<Node> children = new ArrayList<>();

    @Override
    public void add(Node node) {
        children.add(node);
    }

    @Override
    public void remove(Node node) {
        children.remove(node);
    }

    @Override
    public int getValue() {
        return children.stream()
                .mapToInt(Node::getValue)
                .sum();
    }
}
