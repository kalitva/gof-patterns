package com.epam.gof.structural.composite;

public interface Node {

    int getValue();
}
