package com.epam.gof.structural.composite;

public class Client {

    @SuppressWarnings("java:S106")
    public static void main(String[] args) {
        CompositeNode head = new CompositeNodeImpl();
        head.add(new NodeImpl(1));
        head.add(new NodeImpl(2));
        head.add(new NodeImpl(3));
        head.add(new NodeImpl(4));
        head.add(new NodeImpl(5));

        CompositeNode branch = new CompositeNodeImpl();
        branch.add(new NodeImpl(1));
        branch.add(new NodeImpl(2));
        branch.add(new NodeImpl(3));
        branch.add(new NodeImpl(4));

        head.add(branch);

        System.out.println(head.getValue());
    }
}
