package com.epam.gof.structural.composite;

public class NodeImpl implements Node {
    private final int value;

    public NodeImpl(int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }
}
