package com.epam.gof.structural.composite;

public interface CompositeNode extends Node {

    void add(Node node);

    void remove(Node node);
}
