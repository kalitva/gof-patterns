package com.epam.gof.structural.bridge;

public interface Color {

    String fill();
}
