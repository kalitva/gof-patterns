package com.epam.gof.structural.bridge;

public class Red implements Color {

    @Override
    public String fill() {
        return "The color is Red";
    }
}
