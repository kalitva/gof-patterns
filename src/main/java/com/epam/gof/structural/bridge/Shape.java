package com.epam.gof.structural.bridge;

public interface Shape {

    String draw();
}
