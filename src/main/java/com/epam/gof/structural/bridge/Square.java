package com.epam.gof.structural.bridge;

public class Square implements Shape {
    private final Color color;

    public Square(Color color) {
        this.color = color;
    }

    @Override
    public String draw() {
        return "Square has drawn. " + color.fill();
    }
}
