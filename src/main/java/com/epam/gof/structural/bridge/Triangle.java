package com.epam.gof.structural.bridge;

public class Triangle implements Shape {
    private final Color color;

    public Triangle(Color color) {
        this.color = color;
    }

    @Override
    public String draw() {
        return "Triangle has drawn. " + color.fill();
    }
}
