package com.epam.gof.structural.bridge;

public class Client {

    @SuppressWarnings("java:S106")
    public static void main(String[] args) {
        Shape redSquare = new Square(new Red());
        System.out.println(redSquare.draw());

        Shape redTriangle = new Triangle(new Red());
        System.out.println(redTriangle.draw());

        Shape blueSquare = new Square(new Blue());
        System.out.println(blueSquare.draw());
    }
}
