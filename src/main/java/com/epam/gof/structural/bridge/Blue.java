package com.epam.gof.structural.bridge;

public class Blue implements Color {

    @Override
    public String fill() {
        return "The color is Blue.";
    }
}
