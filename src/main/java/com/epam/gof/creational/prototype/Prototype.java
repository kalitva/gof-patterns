package com.epam.gof.creational.prototype;

public interface Prototype<T> {

    T getClone();
}
