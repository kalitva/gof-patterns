package com.epam.gof.creational.prototype;

public class Client {

    @SuppressWarnings("java:S106")
    public static void main(String[] args) {
        Person person = new Person("ivanov", "ivan", "ivano@mail.com");
        Person copy = person.getClone();
        System.out.println("Compare by reference: " + (person == copy));
        System.out.println("Compare by value: " + (person.equals(copy)));
    }
}
