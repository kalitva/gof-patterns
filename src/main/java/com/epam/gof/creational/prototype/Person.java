package com.epam.gof.creational.prototype;

import java.util.Objects;

public class Person implements Prototype<Person> {
    private final String name;
    private final String surname;
    private final String email;

    public Person(String name, String surname, String email) {
        this.name = name;
        this.surname = surname;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public Person getClone() {
        return new Person(name, surname, email);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Person person = (Person) o;
        return name.equals(person.name)
                && surname.equals(person.surname)
                && email.equals(person.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, email);
    }

    @Override
    public String toString() {
        return String.format("Person[name=%s, surname=%s, email=%s]", name, surname, email);
    }
}
