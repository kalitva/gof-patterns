package com.epam.gof.creational.singleton;

@SuppressWarnings({"java:S106"})
public class Printer {

    private Printer() {
        System.out.println("Created printer");
    }

    public static Printer getInstance() {
        return Holder.INSTANCE;
    }

    public static void sayHello() {
        System.out.println("Hello, bro!");
    }

    public void print(String text) {
        System.out.println(text);
    }

    private static class Holder {
        private static final Printer INSTANCE = new Printer();
    }
}
