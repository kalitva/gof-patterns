package com.epam.gof.creational.singleton;

public class Client {

    public static void main(String[] args) {
        // This call won't cause creation of the Printer
        Printer.sayHello();
        // Only this call
        Printer.getInstance().print("Hello, World!");
    }
}
