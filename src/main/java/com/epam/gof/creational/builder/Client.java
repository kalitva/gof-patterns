package com.epam.gof.creational.builder;

/**
 * @see java.lang.StringBuilder
 */
public class Client {

    @SuppressWarnings({"java:S106"})
    public static void main(String[] args) {
        Cake cake = Cake.builder()
                .withButter(200d)
                .withEggs(3)
                .withVanilla(50)
                .withFlour(500d)
                .withMilk(200)
                .withCherry(100)
                .build();
        System.out.println(cake);
    }
}
