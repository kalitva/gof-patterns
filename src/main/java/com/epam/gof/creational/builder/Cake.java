package com.epam.gof.creational.builder;

public class Cake {
    private final double butter;  //cup
    private final int eggs;
    private final int vanilla;     //spoon
    private final double flour;   //cup
    private final double milk;  //cup
    private final int cherry;

    public Cake(Builder builder) {
        butter = builder.butter;
        eggs = builder.eggs;
        vanilla = builder.vanilla;
        flour = builder.flour;
        milk = builder.milk;
        cherry = builder.cherry;
    }

    public static Builder builder() {
        return new Builder();
    }

    public double getButter() {
        return butter;
    }

    public int getEggs() {
        return eggs;
    }

    public int getVanilla() {
        return vanilla;
    }

    public double getFlour() {
        return flour;
    }

    public double getMilk() {
        return milk;
    }

    public int getCherry() {
        return cherry;
    }

    public static class Builder {
        private double butter;
        private int eggs;
        private int vanilla;
        private double flour;
        private double milk;
        private int cherry;

        public Builder withButter(double butter) {
            this.butter = butter;
            return this;
        }

        public Builder withEggs(int eggs) {
            this.eggs = eggs;
            return this;
        }

        public Builder withVanilla(int vanilla) {
            this.vanilla = vanilla;
            return this;
        }

        public Builder withFlour(double flour) {
            this.flour = flour;
            return this;
        }

        public Builder withMilk(double milk) {
            this.milk = milk;
            return this;
        }

        public Builder withCherry(int cherry) {
            this.cherry = cherry;
            return this;
        }

        public Cake build() {
            return new Cake(this);
        }
    }

    @Override
    public String toString() {
        return String.format("Cake[butter=%s, eggs=%s, vanilla=%s, flour=%s, milk=%s, cherry=%s]",
                butter, eggs, vanilla, flour, milk, cherry);
    }
}
