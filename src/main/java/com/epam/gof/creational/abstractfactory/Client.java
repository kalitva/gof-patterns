package com.epam.gof.creational.abstractfactory;

public class Client {

    @SuppressWarnings("java:S106")
    public static void main(String[] args) {
        doThings(new BlueCircleFactory());
        System.out.println();
        doThings(new GreenRectangleFactory());
    }

    private static void doThings(AbstractFactory factory) {
        factory.getShape().drawShape();
        factory.getColor().fillColor();
    }
}
