package com.epam.gof.creational.abstractfactory;

public class Blue implements Color {

    @Override
    @SuppressWarnings("java:S106")
    public void fillColor() {
        System.out.println("Fill blue color");
    }
}
