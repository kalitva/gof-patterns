package com.epam.gof.creational.abstractfactory;

public interface AbstractFactory {

    Color getColor();

    Shape getShape();
}
