package com.epam.gof.creational.abstractfactory;

public interface Color {

    void fillColor();
}
