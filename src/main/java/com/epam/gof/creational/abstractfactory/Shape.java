package com.epam.gof.creational.abstractfactory;

public interface Shape {

    void drawShape();
}
