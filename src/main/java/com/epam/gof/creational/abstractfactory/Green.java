package com.epam.gof.creational.abstractfactory;

public class Green implements Color {

    @Override
    @SuppressWarnings("java:S106")
    public void fillColor() {
        System.out.println("Fill green color");
    }
}
