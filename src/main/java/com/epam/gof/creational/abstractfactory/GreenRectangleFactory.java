package com.epam.gof.creational.abstractfactory;

public class GreenRectangleFactory implements AbstractFactory {
    private static final Color GREEN_COLOR = new Green();
    private static final Shape RECTANGLE_SHAPE = new Rectangle();

    @Override
    public Color getColor() {
        return GREEN_COLOR;
    }

    @Override
    public Shape getShape() {
        return RECTANGLE_SHAPE;
    }
}
