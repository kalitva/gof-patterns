package com.epam.gof.creational.abstractfactory;

public class BlueCircleFactory implements AbstractFactory {
    private static final Color BLUE_COLOR = new Blue();
    private static final Shape CIRCLE_SHAPE = new Circle();

    @Override
    public Color getColor() {
        return BLUE_COLOR;
    }

    @Override
    public Shape getShape() {
        return CIRCLE_SHAPE;
    }
}
