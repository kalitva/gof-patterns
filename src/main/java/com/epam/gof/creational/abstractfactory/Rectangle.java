package com.epam.gof.creational.abstractfactory;

public class Rectangle implements Shape {

    @Override
    @SuppressWarnings("java:S106")
    public void drawShape() {
        System.out.println("Shape Rectangle");
    }
}
