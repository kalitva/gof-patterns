package com.epam.gof.creational.factorymethod;

public class PlainWriter extends AbstractWriter {

    @Override
    protected Encoder getEncoder() {
        return text -> text;
    }
}
