package com.epam.gof.creational.factorymethod;

import java.util.Base64;

public class EncodedWriter extends AbstractWriter {

    @Override
    protected Encoder getEncoder() {
        return text -> Base64.getEncoder().encodeToString(text.getBytes());
    }
}
