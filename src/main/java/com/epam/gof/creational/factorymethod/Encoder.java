package com.epam.gof.creational.factorymethod;

public interface Encoder {

    String encrypt(String text);
}
