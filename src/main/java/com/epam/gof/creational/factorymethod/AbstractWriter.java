package com.epam.gof.creational.factorymethod;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UncheckedIOException;

public abstract class AbstractWriter {

    public void writeToFile(String text, String filename) {
        String encodedText = getEncoder().encrypt(text);
        try (OutputStream outputStream = new FileOutputStream(filename)) {
            outputStream.write(encodedText.getBytes());
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    protected abstract Encoder getEncoder();
}
