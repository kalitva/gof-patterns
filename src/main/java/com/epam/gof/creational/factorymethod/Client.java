package com.epam.gof.creational.factorymethod;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Client {
    private static final String FILE_NAME = "file.txt";

    @SuppressWarnings("java:S106")
    public static void main(String[] args) throws IOException {
        PlainWriter plainWriter = new PlainWriter();
        plainWriter.writeToFile("Hello world!", FILE_NAME);
        System.out.println(Files.readString(Path.of(FILE_NAME)));

        EncodedWriter encodedWriter = new EncodedWriter();
        encodedWriter.writeToFile("Hello world!", FILE_NAME);
        System.out.println(Files.readString(Path.of(FILE_NAME)));
    }
}
