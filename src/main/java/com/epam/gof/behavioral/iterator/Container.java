package com.epam.gof.behavioral.iterator;

public interface Container<T> {

    Iterator<T> iterator();
}
