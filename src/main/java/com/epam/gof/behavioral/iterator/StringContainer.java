package com.epam.gof.behavioral.iterator;

public class StringContainer implements Container<String> {
    private final String[] strings;

    public StringContainer(String[] strings) {
        this.strings = strings;
    }

    @Override
    public Iterator<String> iterator() {
        return new Iterator<>() {
            private int index;

            @Override
            public boolean hasNext() {
                return index < strings.length;
            }

            @Override
            public String next() {
                return strings[index++];
            }
        };
    }
}
