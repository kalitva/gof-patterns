package com.epam.gof.behavioral.iterator;

public interface Iterator<T> {

    boolean hasNext();

    T next();
}
