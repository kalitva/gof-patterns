package com.epam.gof.behavioral.iterator;

/**
 * @see java.util.Iterator
 */
public class Main {

    @SuppressWarnings("java:S106")
    public static void main(String[] args) {
        StringContainer container = new StringContainer(new String[]{"Robert", "John", "Julie", "Lora", "Adel"});
        Iterator<String> iterator = container.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }
}
