package com.epam.gof.behavioral.templatemethod;

public abstract class CrossCompiler {

    public final void crossCompile() {
        collectSource();
        compileToTarget();
    }

    @SuppressWarnings("java:S106")
    protected void collectSource() {
        System.out.println("collect sources");
    }

    //Template methods
    protected abstract void compileToTarget();
}
