package com.epam.gof.behavioral.templatemethod;

public class AndroidCompiler extends CrossCompiler {

    @Override
    @SuppressWarnings("java:S106")
    protected void compileToTarget() {
        System.out.println("android compile to target");
    }
}
