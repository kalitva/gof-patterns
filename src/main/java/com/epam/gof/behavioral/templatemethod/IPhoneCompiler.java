package com.epam.gof.behavioral.templatemethod;

public class IPhoneCompiler extends CrossCompiler {

    @Override
    @SuppressWarnings("java:S106")
    protected void compileToTarget() {
        System.out.println("iphone compile to target");
    }
}
