package com.epam.gof.behavioral.state;

public interface State {

    void stop();

    void start();

    void pause();
}
