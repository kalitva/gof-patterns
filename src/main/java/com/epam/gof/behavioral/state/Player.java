package com.epam.gof.behavioral.state;

public class Player {
    private State state;

    public Player() {
        state = new StoppedState(this);
    }

    public void start() {
        state.start();
    }

    public void stop() {
        state.stop();
    }

    public void pause() {
        state.pause();
    }

    public void setState(State state) {
        this.state = state;
    }
}
