package com.epam.gof.behavioral.state;

public class StoppedState implements State {
    private final Player player;

    public StoppedState(Player player) {
        this.player = player;
    }

    @Override
    public void stop() {
        // Do nothing, already stopped
    }

    @Override
    public void start() {
        System.out.println("Playing...");
        player.setState(new PlayingState(player));
    }

    @Override
    public void pause() {
        // Ignore
    }
}
