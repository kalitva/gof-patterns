package com.epam.gof.behavioral.state;

public class Main {

    public static void main(String[] args) {
        Player player = new Player();
        player.start();
        player.start();

        player.pause();
        player.pause();
        player.stop();
        player.start();
        player.stop();
    }
}
