package com.epam.gof.behavioral.state;

public class PlayingState implements State {
    private final Player player;

    public PlayingState(Player player) {
        this.player = player;
    }

    @Override
    public void stop() {
        System.out.println("Stopped...");
        player.setState(new StoppedState(player));
    }

    @Override
    public void start() {
        // Ignore
    }

    @Override
    public void pause() {
        System.out.println("Paused...");
        player.setState(new PausedState(player));
    }
}
