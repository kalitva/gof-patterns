package com.epam.gof.behavioral.state;

public class PausedState implements State {
    private final Player player;

    public PausedState(Player player) {
        this.player = player;
    }

    @Override
    public void stop() {
        // Ignore
    }

    @Override
    public void start() {
        System.out.println("Playing...");
        player.setState(new PlayingState(player));
    }

    @Override
    public void pause() {
        // Ignore
    }
}
