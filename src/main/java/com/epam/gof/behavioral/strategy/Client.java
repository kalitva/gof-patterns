package com.epam.gof.behavioral.strategy;

import java.util.Collections;

/**
 * @see java.util.Comparator, executed by among others Collections#sort().
 */
public class Client {

    public static void main(String[] args) {
        CompressionContext context = new CompressionContext();
        context.setCompressionStrategy(new ZipCompressionStrategy());
        context.createArchive(Collections.emptyList());
    }
}
