package com.epam.gof.behavioral.strategy;

import java.io.File;
import java.util.List;

public class CompressionContext {
    private CompressionStrategy compressionStrategy;

    // this can be set at runtime by the application preferences
    public void setCompressionStrategy(CompressionStrategy compressionStrategy) {
        this.compressionStrategy = compressionStrategy;
    }

    public void createArchive(List<File> files) {
        compressionStrategy.compressFiles(files);
    }
}
