package com.epam.gof.behavioral.visitor;

public class CD implements Visitable {
    private final double price;
    private final double weight;

    public CD(double price, double weight) {
        this.price = price;
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public double getWeight() {
        return weight;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }
}
