package com.epam.gof.behavioral.visitor;

public class TotalPriceVisitor implements Visitor {
    private double totalPrice;

    @Override
    public void visit(Book book) {
        totalPrice += book.getPrice();
    }

    @Override
    public void visit(CD cd) {
        totalPrice += cd.getPrice();
    }

    @Override
    public void visit(DVD dvd) {
        totalPrice += dvd.getPrice();
    }

    public double getTotalPrice() {
        return totalPrice;
    }
}
