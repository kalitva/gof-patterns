package com.epam.gof.behavioral.visitor;

public class TotalWeightVisitor implements Visitor {
    private double totalWeight;

    @Override
    public void visit(Book book) {
        totalWeight += book.getWeight();
    }

    @Override
    public void visit(CD cd) {
        totalWeight += cd.getWeight();
    }

    @Override
    public void visit(DVD dvd) {
        totalWeight += dvd.getWeight();
    }

    public double getTotalWeight() {
        return totalWeight;
    }
}
