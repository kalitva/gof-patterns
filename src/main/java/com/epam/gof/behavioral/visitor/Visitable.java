package com.epam.gof.behavioral.visitor;

public interface Visitable {

    void accept(Visitor visitor);

}
