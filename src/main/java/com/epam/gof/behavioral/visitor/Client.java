package com.epam.gof.behavioral.visitor;

import java.util.List;

/**
 * @see java.nio.file.FileVisitor
 * @see javax.lang.model.element.ElementVisitor
 */
public class Client {

    @SuppressWarnings("java:S106")
    public static void main(String[] args) {
        List<Visitable> items = List.of(new Book(2, 3), new CD(3, 4), new DVD(5, 5));

        TotalWeightVisitor weightVisitor = new TotalWeightVisitor();
        items.forEach(i -> i.accept(weightVisitor));
        System.out.println("Total weight: " + weightVisitor.getTotalWeight());

        TotalPriceVisitor priceVisitor = new TotalPriceVisitor();
        items.forEach(i -> i.accept(priceVisitor));
        System.out.println("Total price: " + priceVisitor.getTotalPrice());
    }
}
