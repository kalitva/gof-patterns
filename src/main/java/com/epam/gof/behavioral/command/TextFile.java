package com.epam.gof.behavioral.command;

import java.util.ArrayList;
import java.util.List;

public class TextFile {
    private final List<String> text = new ArrayList<>();

    private final String filename;

    public TextFile(String filename) {
        this.filename = filename;
    }

    public void writeLine(WriteLineCommand command) {
        text.add(command.getIndex(), command.getLine());
    }

    public void removeLine(RemoveLineCommand command) {
        text.remove(command.getIndex());
    }

    public String print() {
        return "------------ " + filename + " ------------\n" +
                String.join(System.lineSeparator(), text);
    }

    public String getLine(int index) {
        return text.get(index);
    }

    public int lines() {
        return text.size();
    }
}
