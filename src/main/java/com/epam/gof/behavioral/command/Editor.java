package com.epam.gof.behavioral.command;

import java.util.ArrayList;
import java.util.List;

public class Editor {
    private final List<Command> commands = new ArrayList<>();

    void executeCommand(Command command) {
        commands.add(command);
        command.execute();
    }

    void undoCommand() {
        commands.remove(commands.size() - 1)
                .undo();
    }
}
