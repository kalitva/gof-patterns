package com.epam.gof.behavioral.command;

public interface Command {

    void execute();

    void undo();
}
