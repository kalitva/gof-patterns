package com.epam.gof.behavioral.command;

public class WriteLineCommand implements Command {
    private final TextFile textFile;
    private final String line;
    private final int index;

    public WriteLineCommand(TextFile textFile, String line, int index) {
        this.textFile = textFile;
        this.line = line;
        this.index = index;
    }

    @Override
    public void execute() {
        textFile.writeLine(this);
    }

    @Override
    public void undo() {
        textFile.removeLine(new RemoveLineCommand(textFile, index));
    }

    public String getLine() {
        return line;
    }

    public int getIndex() {
        return index;
    }
}
