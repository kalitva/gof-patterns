package com.epam.gof.behavioral.command;

public class RemoveLineCommand implements Command {
    private final TextFile textFile;
    private final int index;
    private final String removedLine;

    public RemoveLineCommand(TextFile textFile, int index) {
        this.textFile = textFile;
        this.index = index;
        this.removedLine = textFile.getLine(index);
    }

    @Override
    public void execute() {
        textFile.removeLine(this);
    }

    @Override
    public void undo() {
        textFile.writeLine(new WriteLineCommand(textFile, removedLine, index));
    }

    public int getIndex() {
        return index;
    }
}
