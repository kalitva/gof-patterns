package com.epam.gof.behavioral.command;

/**
 * @see javax.swing.Action
 */
public class Main {

    @SuppressWarnings("java:S106")
    public static void main(String[] args) {
        TextFile textFile = new TextFile("Some text file");
        Editor editor = new Editor();
        editor.executeCommand(new WriteLineCommand(textFile, "1 line", 0));
        editor.executeCommand(new WriteLineCommand(textFile, "2 line", 1));
        editor.executeCommand(new WriteLineCommand(textFile, "3 line", 2));
        System.out.println(textFile.print());

        editor.undoCommand();
        System.out.println(textFile.print());

        editor.executeCommand(new WriteLineCommand(textFile, "3 line", 2));
        editor.executeCommand(new WriteLineCommand(textFile, "4 line", 3));
        editor.executeCommand(new RemoveLineCommand(textFile, 0));
        System.out.println(textFile.print());

        editor.undoCommand();
        System.out.println(textFile.print());
    }
}
