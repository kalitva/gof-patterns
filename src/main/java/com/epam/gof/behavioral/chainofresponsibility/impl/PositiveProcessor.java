package com.epam.gof.behavioral.chainofresponsibility.impl;

import com.epam.gof.behavioral.chainofresponsibility.ChainLink;
import com.epam.gof.behavioral.chainofresponsibility.Request;

public class PositiveProcessor implements ChainLink {

    @Override
    public void process(Request request) {
        System.out.println("Positive processor: " + request.getNumber());
    }

    @Override
    public boolean test(Request request) {
        return request.getNumber() > 0;
    }
}
