package com.epam.gof.behavioral.chainofresponsibility.impl;

import com.epam.gof.behavioral.chainofresponsibility.ChainLink;
import com.epam.gof.behavioral.chainofresponsibility.Request;

public class NegativeProcessor implements ChainLink {

    @Override
    public void process(Request request) {
        System.out.println("Negative processor: " + request.getNumber());
    }

    @Override
    public boolean test(Request request) {
        return request.getNumber() < 0;
    }
}
