package com.epam.gof.behavioral.chainofresponsibility;

import java.util.LinkedList;
import java.util.List;

public class Chain {
    private final List<ChainLink> links;

    private Chain(List<ChainLink> link) {
        this.links = link;
    }

    public static ChainBuilder builder() {
        return new ChainBuilder();
    }

    public final void process(Request request) {
        links.stream()
                .filter(l -> l.test(request))
                .forEach(l -> l.process(request));
    }

    public static class ChainBuilder {
        private final List<ChainLink> links = new LinkedList<>();

        public ChainBuilder withNext(ChainLink link) {
            links.add(link);
            return this;
        }

        public Chain build() {
            return new Chain(links);
        }
    }
}
