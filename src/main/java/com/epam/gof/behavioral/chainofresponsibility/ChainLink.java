package com.epam.gof.behavioral.chainofresponsibility;

public interface ChainLink {

    void process(Request request);

    boolean test(Request request);
}
