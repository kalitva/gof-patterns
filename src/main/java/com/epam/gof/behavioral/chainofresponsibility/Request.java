package com.epam.gof.behavioral.chainofresponsibility;

public class Request {
    private final int number;

    public Request(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }
}
