package com.epam.gof.behavioral.chainofresponsibility;

import com.epam.gof.behavioral.chainofresponsibility.impl.DefaultProcessor;
import com.epam.gof.behavioral.chainofresponsibility.impl.NegativeProcessor;
import com.epam.gof.behavioral.chainofresponsibility.impl.PositiveProcessor;
import com.epam.gof.behavioral.chainofresponsibility.impl.ZeroProcessor;

/**
 * @see javax.servlet.Filter#doFilter
 */
public class Main {

    public static void main(String[] args) {
        Chain chain = Chain.builder()
                .withNext(new NegativeProcessor())
                .withNext(new ZeroProcessor())
                .withNext(new PositiveProcessor())
                .withNext(new DefaultProcessor())
                .build();

        chain.process(new Request(99));
        chain.process(new Request(-30));
        chain.process(new Request(0));
        chain.process(new Request(100));
    }
}
