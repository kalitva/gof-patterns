package com.epam.gof.behavioral.memento;

public class EditorSnapshot {
    private final Editor editor;
    private final String text;

    public EditorSnapshot(Editor editor, String text) {
        this.editor = editor;
        this.text = text;
    }

    public void restore() {
        editor.setText(text);
    }
}
