package com.epam.gof.behavioral.memento;

import java.util.ArrayList;
import java.util.List;

public class EditorHistory {
    private final List<EditorSnapshot> mementos = new ArrayList<>();

    private final Editor editor;

    public EditorHistory(Editor editor) {
        this.editor = editor;
    }

    public void makeSnapshot() {
        mementos.add(editor.snapshot());
    }

    public void undo() {
        if (!mementos.isEmpty()) {
            mementos.remove(mementos.size() - 1).restore();
        }
    }
}
