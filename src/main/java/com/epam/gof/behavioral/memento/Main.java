package com.epam.gof.behavioral.memento;

public class Main {

    public static void main(String[] args) {
        Editor editor = new Editor();
        EditorHistory history = new EditorHistory(editor);

        editor.setText("Hello 1");
        history.makeSnapshot();
        editor.setText("Hello 2");
        history.makeSnapshot();
        editor.print();

        editor.setText("Hello 3");
        history.undo();
        history.undo();
        editor.print();
    }
}
