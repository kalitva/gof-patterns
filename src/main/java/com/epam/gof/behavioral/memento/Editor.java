package com.epam.gof.behavioral.memento;

public class Editor {
    // State. Here may and should be more fields
    private String text;

    public void setText(String text) {
        this.text = text;
    }

    public void print() {
        System.out.println(text);
    }

    public EditorSnapshot snapshot() {
        return new EditorSnapshot(this, text);
    }
}
