package com.epam.gof.behavioral.observer;

public class Follower {
    private final String name;

    public Follower(String name) {
        this.name = name;
    }

    public void notification(String speaker, String tweet) {
        System.out.printf("'%s' received notification from speaker: '%s', Tweet: '%s'", name, speaker, tweet);
        System.out.println();
    }
}
