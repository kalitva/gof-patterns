package com.epam.gof.behavioral.observer;

public class Main {

    public static void main(String[] args) {
        PublicFigure bobama = new PublicFigure("Barack Obama", "bobama");
        PublicFigure nmodi = new PublicFigure("Narendra Modi", "nmodi");

        Follower ajay = new Follower("Ajay");
        Follower vijay = new Follower("Vijay");
        Follower racheal = new Follower("Racheal");
        Follower micheal = new Follower("Micheal");
        Follower kim = new Follower("Kim");

        bobama.addFollower(ajay);
        bobama.addFollower(vijay);
        bobama.addFollower(racheal);
        bobama.addFollower(micheal);
        bobama.addFollower(kim);

        nmodi.addFollower(ajay);
        nmodi.addFollower(vijay);
        nmodi.addFollower(racheal);
        nmodi.addFollower(micheal);
        nmodi.addFollower(kim);

        bobama.tweet("Hello Friends!");
        nmodi.tweet("Vande Matram!");
        bobama.removeFollower(racheal);
        bobama.tweet("Stay Home! Stay Safe!");
    }
}
