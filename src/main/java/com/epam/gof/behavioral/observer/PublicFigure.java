package com.epam.gof.behavioral.observer;

import java.util.HashSet;
import java.util.Set;

public class PublicFigure {
    private final Set<Follower> followers = new HashSet<>();
    private final String name;
    private final String nickname;

    public PublicFigure(String name, String nickname) {
        this.name = name;
        this.nickname = "#" + nickname;
    }

    public String getName() {
        return name;
    }

    public void tweet(String tweet) {
        System.out.printf("\nName: %s, Tweet: %s\n", name, tweet);
        followers.forEach(f -> f.notification(nickname, tweet));
    }

    public synchronized void addFollower(Follower f) {
        followers.add(f);
    }

    public synchronized void removeFollower(Follower f) {
        followers.remove(f);
    }
}
