package com.epam.gof.behavioral.mediator;

import java.util.ArrayList;
import java.util.List;

public class ChatMediator {
    private final List<User> users = new ArrayList<>();

    public void addUser(User user) {
        users.add(user);
    }

    public void send(String message, User originator) {
        users.stream()
                .filter(u -> u != originator)
                .forEach(u -> u.receive(message));
    }
}
