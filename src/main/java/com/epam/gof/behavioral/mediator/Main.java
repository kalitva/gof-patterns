package com.epam.gof.behavioral.mediator;

/**
 * @see java.util.Timer#schedule
 * @see java.util.concurrent.Executor#execute
 */
public class Main {

    public static void main(String[] args) {
        ChatMediator chat = new ChatMediator();
        User user1 = new User(chat, "Pankaj");
        User user2 = new User(chat, "Lisa");
        User user3 = new User(chat, "Saurabh");
        User user4 = new User(chat, "David");
        chat.addUser(user1);
        chat.addUser(user2);
        chat.addUser(user3);
        chat.addUser(user4);
        user1.send("Hi All");
        user2.send("Hi All");
    }
}
