package com.epam.gof.behavioral.mediator;

public class User {
    private final ChatMediator chatMediator;
    private final String name;

    protected User(ChatMediator chatMediator, String name) {
        this.chatMediator = chatMediator;
        this.name = name;
    }

    public void send(String message) {
        System.out.println(name + " sending a message...");
        chatMediator.send(message, this);
        System.out.println();
    }

    public void receive(String message) {
        System.out.println(name + " received '" + message +"'");
    }
}
