package com.epam.gof.behavioral.interpreter.impl;

import com.epam.gof.behavioral.interpreter.Expression;

public class NumberExpression implements Expression {
    private final int number;

    public NumberExpression(int number) {
        this.number = number;
    }

    public NumberExpression(String s) {
        this.number = Integer.parseInt(s);
    }

    @Override
    public int interpret() {
        return number;
    }
}
