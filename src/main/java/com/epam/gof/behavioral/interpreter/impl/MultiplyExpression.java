package com.epam.gof.behavioral.interpreter.impl;

import com.epam.gof.behavioral.interpreter.Expression;

public class MultiplyExpression implements Expression {
    private final Expression leftExpression;
    private final Expression rightExpression;

    public MultiplyExpression(Expression leftExpression, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public int interpret() {
        return leftExpression.interpret() * rightExpression.interpret();
    }
}
