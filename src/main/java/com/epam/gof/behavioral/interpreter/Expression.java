package com.epam.gof.behavioral.interpreter;

public interface Expression {

    int interpret();
}
