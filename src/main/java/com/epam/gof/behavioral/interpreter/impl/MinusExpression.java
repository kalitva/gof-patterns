package com.epam.gof.behavioral.interpreter.impl;

import com.epam.gof.behavioral.interpreter.Expression;

public class MinusExpression implements Expression {
    private final Expression leftExpression;
    private final Expression rightExpression;

    public MinusExpression(Expression leftExpression, Expression rightExpression) {
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public int interpret() {
        return leftExpression.interpret() - rightExpression.interpret();
    }
}
