package com.epam.gof.behavioral.interpreter;

public class Main {

    public static void main(String[] args) {
        ExpressionParser ep = new ExpressionParser();
        System.out.println(ep.parse("4 3 2 - 1 + *"));
        System.out.println(ep.parse("23 3 +"));
        System.out.println(ep.parse("5 3 2 - 1 + * 1 + "));
    }
}
