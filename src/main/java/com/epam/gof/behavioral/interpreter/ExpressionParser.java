package com.epam.gof.behavioral.interpreter;

import com.epam.gof.behavioral.interpreter.impl.MinusExpression;
import com.epam.gof.behavioral.interpreter.impl.MultiplyExpression;
import com.epam.gof.behavioral.interpreter.impl.NumberExpression;
import com.epam.gof.behavioral.interpreter.impl.PlusExpression;

import java.util.ArrayDeque;
import java.util.Deque;

public class ExpressionParser {

    public int parse(String tokenString) {
        Deque<Expression> stack = new ArrayDeque<>();
        String[] tokens = tokenString.split(" ");

        for (String t : tokens) {
            if (isNumeric(t)) {
                stack.push(new NumberExpression(t));
            } else {
                Expression right = stack.pop();
                Expression left = stack.pop();
                Expression operator = getOperatorInstance(t, left, right);
                stack.push(new NumberExpression(operator.interpret()));
            }
        }

        return stack.pop().interpret();
    }

    private boolean isNumeric(String token) {
        return token.chars().allMatch(Character::isDigit);
    }

    private Expression getOperatorInstance(String token, Expression left, Expression right) {
        switch (token) {
            case "+":
                return new PlusExpression(left, right);
            case "-":
                return new MinusExpression(left, right);
            case "*":
                return new MultiplyExpression(left, right);
            default:
                throw new RuntimeException("There is no expression " + token);
        }
    }
}
